import React from 'react';
import QdtComponent from './components/qdt';
import './App.css';

const viz1 = {
  type: 'QdtViz',
  props: {
    type: 'barchart', 
    id: 'a5e0f12c-38f5-4da9-8f3f-0e4566b28398', 
    height: '300px',      
    exportData: true,
    exportImg: true,
    exportImgOptions: { width: 600, height: 400, format: 'JPG' },
    exportPdf: true,
    exportPdfOptions: { documentSize: { width: 300, height: 150 } },
  },
};

function App() {
  return (
    <QdtComponent type={viz1.type} props={viz1.props} />
  );
}

export default App;
